﻿#include <stdio.h>
#include <stdlib.h>
#include "headers/h_recherche.h"

bool solutionLargeur(Noeud * pDepart, Noeud * pSolution,
                     void * pProbleme,
                     bool (*estSolution)(Noeud *, Noeud *),
                     void (*recupSucc)(Noeud *, File *, void *)) {
    bool solution       = false;
    File aParcourir     = creerFile();
    Liste listeMarques  = creerListe();
    Noeud * courant     = pDepart;
    courant->pere       = NULL;

    do {
        if( estSolution(courant, pSolution) ) {
            solution = true;
        }
        if (!estPresentListe(courant, &listeMarques)) {
            // On récupère les successeurs du noeud courant
            recupSucc(courant, &aParcourir, pProbleme);
            // On marque le noeud courant
            ajouterFinListe(courant, &listeMarques);
        }
        courant = retirerFile(&aParcourir);
    } while(!solution && courant != NULL );
    return solution;
}

Liste cheminLargeur(Noeud * pDepart, Noeud * pSolution,
                     void * pProbleme,
                     bool (*estSolution)(Noeud *, Noeud *),
                     void (*recupSucc)(Noeud *, File *, void *)) {

    bool solution       = false;
    File aParcourir     = creerFile();
    Liste listeChemin   = creerListe();
    Liste listeMarques  = creerListe();
    Noeud * courant     = pDepart;
    courant->pere       = NULL;

    do {
        if( estSolution(courant, pSolution) ) {
            solution = true;
        }
        if (!estPresentListe(courant, &listeMarques)) {
            // On récupère les successeurs du noeud courant
            recupSucc(courant, &aParcourir, pProbleme);
            // On marque le noeud courant
            ajouterFinListe(courant, &listeMarques);
        }
        courant = retirerFile(&aParcourir);
    } while(!solution && courant != NULL );

    if (solution) {
        ajouterFinListe(courant, &listeChemin);

        while(courant->pere != NULL) {
            courant = courant->pere;
            ajouterFinListe(courant, &listeChemin);
        }
    }

    return solution ? listeChemin : creerListe();
}
