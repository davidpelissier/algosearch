﻿#include <stdlib.h>
#include "headers/h_file.h"


File creerFile(void) {
    File fl;
    fl.tete   = NULL; // A la création, la tête ne pointe sur aucun élément.
    fl.queue  = NULL; // A la création, la queue ne pointe sur aucun élément.
    fl.taille = 0;
    return fl ;
};


void viderFile(File * pFile) {
    // Si la file existe
    if (pFile != NULL) {
        Noeud  * elem;
        // Tant que la tête pointe sur un élément
        do {
            elem = retirerFile(pFile);
        } while(pFile->tete != NULL && elem != NULL);
    }
}


void ajouterFile(Noeud * pElem, File * pFile) {
    // Si la file et l'élement de la file existent
    if ( (pFile != NULL) && (pElem != NULL) ) {

        // Creation d'une zone mémoire dynamique pour l'élément de la liste
        Noeud * nouvelElem = (Noeud *) malloc( sizeof(Noeud));

        // Si le nouvel élément a bien été créé
        if(nouvelElem != NULL) {
            nouvelElem->contenu    = pElem->contenu;
            nouvelElem->successeur = pElem->successeur;
            nouvelElem->pere       = pElem->pere;

            // Si la queue de file existe
            if (pFile->queue != NULL) {
                pFile->queue->successeur = nouvelElem;
            }
            // Pointage de la nouvelle queue
            pFile->queue = nouvelElem;

            // Si la file était vide la tête = la queue
            if(pFile->tete == NULL) {
                pFile->tete = nouvelElem;
            }
            pFile->taille++;
        }
    }
}


Noeud * retirerFile(File * pFile) {
    // Creation d'une zone mémoire dynamique pour l'élément de la liste
    Noeud * nouvelElem = (Noeud *) malloc( sizeof(Noeud));
    // Si la file et l'élement de la file existent
    if ( pFile != NULL ) {
        // Si la file est non vide
        if ( !estFileVide(pFile) ) {
            nouvelElem->contenu    = NULL;
            nouvelElem->successeur = NULL;
            nouvelElem->pere       = NULL;

            // Sauvegarde de l'élément à retirer
            nouvelElem->contenu    = pFile->tete->contenu;
            nouvelElem->successeur = pFile->tete->successeur;
            nouvelElem->pere       = pFile->tete->pere;

            // Définition de la nouvelle tęte
            if (pFile->tete->successeur != NULL) {
                pFile->tete = pFile->tete->successeur;
            } else {
                viderFile(pFile);
                pFile->tete = NULL;
            }
            pFile->taille--;
        } else {
            nouvelElem = NULL;
        }
    } else {
        nouvelElem = NULL;
    }
    return nouvelElem;
}


bool estFileVide(File * pFile) {
    return (pFile->taille == 0);
}
