﻿#include <stdlib.h>
#include <stdio.h>
#include "headers/h_liste.h"


Liste creerListe(void){
    Liste li;
    li.premierElem = NULL;   // Liste vide = pas de premier élément
    li.taille = 0;
    return li ;
}


bool estListeVide(Liste * pListe) {
    return pListe->taille == 0;
}


void ajouterDebListe(Noeud * pElem, Liste * pListe) {
    // Si la liste et l'élément existe
    if ( (pListe != NULL) && pElem != NULL) {
        Noeud * nouvelElem = (Noeud *) malloc( sizeof(Noeud) );
        nouvelElem->contenu    = pElem->contenu;
        nouvelElem->pere       = pElem->pere;
        nouvelElem->successeur = NULL;

        // Si la liste n'est pas vide
        if (!estListeVide(pListe)) {
            nouvelElem->successeur = pListe->premierElem;
            pListe->premierElem = nouvelElem;
        } else {
             pListe->premierElem = nouvelElem;
        }
        pListe->taille++;
    }
 }


void ajouterFinListe(Noeud * pElem, Liste * pListe) {
    // Si la liste et l'élément existe
    if ( (pListe != NULL) && pElem != NULL) {
        Noeud * nouvelElem = (Noeud *) malloc( sizeof(Noeud) );
        nouvelElem->contenu    = pElem->contenu;
        nouvelElem->pere       = pElem->pere;
        nouvelElem->successeur = NULL;

        // Si la liste n'est pas vide
        if (!estListeVide(pListe)) {
            Noeud * dernierElem = iElemListe(pListe->taille, pListe);
            dernierElem->successeur = nouvelElem;
        } else {
            pListe->premierElem = nouvelElem;
        }
        pListe->taille++;
    }
 }


Noeud * iElemListe(int pIndice, Liste * pListe) {
    Noeud * courant = (Noeud *) malloc( sizeof(Noeud) );
    courant = pListe->premierElem;
    if(pIndice > 0) {
        // On se déplace de i éléments, tant que c'est possible
        for(int i = 1; i < pIndice && pListe != NULL; i++) {
            courant = courant->successeur;
        }
    }

    // Si l'élément est NULL, c'est que la liste contient moins de i éléments
    if(courant == NULL) {
        return NULL;
    // Sinon on renvoie l'adresse de l'élément i
    } else {
        return courant;
    }
    free(courant);
    courant = NULL;
}

bool estPresentListe(Noeud * pElem, Liste * pListe) {
    Noeud * courant = (Noeud *) malloc( sizeof(Noeud) );
    courant = pListe->premierElem;
    while (courant != NULL) {
        if(courant->contenu == pElem->contenu) {
            return true;
        }
        courant = courant->successeur;
    }
    return false;
}
