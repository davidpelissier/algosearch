﻿/**
 * @mainpage algoSearch
 *
 * La résolution de problèmes est un champ particulier de l’intelligence
 * artificielle (IA).
 * Il s’agit d’un ensemble de concepts et d’algorithmes
 * génériques, c’est-à-dire qui sont utilisables pour résoudre un grand nombre
 * de problèmes différents.

 * Nous allons nous intéresser plus spécifiquement
 * aux problèmes de recherche et aux algorithmes génériques de recherche de
 * solutions.
 *
 * Un problème de recherche est un problème que l’on peut résoudre en
 * recherchant une solution (état particulier) parmi un ensemble d’alternatives.
 * La recherche correspond alors à  un parcours d’état en état ; d’un état
 * initial (départ) vers un état final (solution).
 *
 * La représentation de la résolution du problème correspond à un graphe où les
 * noeuds sont les étapes de la résolution et les arcs sont les opérateurs de
 * changement d’état.
 *
 * La recherche de solution correspond donc à un parcours de graphe, de l’état
 * initial vers un ou plusieurs états finaux. Si il y a plusieurs états finaux,
 * on peut vouloir soit n’importe lequel, soit tous, soit un en particulier, par
 * exemple, le meilleur, c’est-à-dire la solution optimale.
 * Selon le problème, la solution recherchée peut également être soit l’état
 * final trouvé (par exemple un planning de cours rempli pour un problème de
 * planification), soit le chemin pour arriver à l’état final (par exemple un
 * chemin entre le départ et l’arrivée pour un problème de recherche
 * d’itinéraire).
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "headers/h_noeud.h"
#include "headers/h_file.h"
#include "tests/h_tests_algo.h"
#include "tests/h_tests_file.h"
#include "tests/h_tests_liste.h"

#define DEBUG  true


void saisie_securisee(void);
char menu(void);


int main(void) {

    if(DEBUG) {
        char action;

        while(action != 'q') {
            action = menu();

            switch (action) {
                case 'R':
                    testsAlgo();
                    system("pause");
                    break;
                case 'F':
                    testsFile();
                    system("pause");
                    break;
                case 'L':
                    testsListe();
                    system("pause");
                    break;
                case 'Q':
                    break;
            }
        }
    }
    return 0;
}


char menu(void) {
    system("cls");
    printf("\n\n **** PROGRAMME DE TEST ****");
    char choix;

    printf("\n\n R : Test de l'algorithme ParcoursLargeur");
    printf("\n F : Test structure file");
    printf("\n L : Test structure liste");
    printf("\n Q : Fermer le programme de test");
    printf("\nEntrez votre choix : ");
    scanf("%c",&choix);

    saisie_securisee();
    system("cls");
    return choix;
}


void saisie_securisee(void) {
    int c;
    while ((c = getchar ()) != '\n' && c != EOF);
}
