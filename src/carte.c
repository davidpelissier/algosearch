#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "headers/h_carte.h"

void lireCarte(FILE * pCarte, Probleme * pProbleme) {
    char c;
    pProbleme->depart.motif = ' ';
    fscanf(pCarte,"%i\t%i\n", &pProbleme->depart.x, &pProbleme->depart.y);
    pProbleme->arrivee.motif = ' ';
    fscanf(pCarte,"%i\t%i\n", &pProbleme->arrivee.x, &pProbleme->arrivee.y);
    fscanf(pCarte,"%i\n", &pProbleme->nbLignes);
    fscanf(pCarte,"%i\n", &pProbleme->nbColonnes);

    pProbleme->carte = (Case **) malloc(sizeof(Case *)*pProbleme->nbLignes);
    if(pProbleme->carte == NULL) {
        printf("\nAllocation impossible, pas assez de memoire\n");
        exit(1);
    } else {
        for (int i = 0; i < pProbleme->nbLignes; i++) {
            pProbleme->carte[i] = (Case *) malloc(sizeof(Case)*pProbleme->nbColonnes);
            if (pProbleme->carte[i] == NULL) {
                printf("\nAllocation impossible, pas assez de memoire\n");
                exit (1);
            }
        }
    }
    for (int i = 0; i < pProbleme->nbLignes; i++) {
        for(int j = 0; j< pProbleme->nbColonnes; j++) {
            pProbleme->carte[i][j].x = i;
            pProbleme->carte[i][j].y = j;
            fscanf(pCarte, "%c", &pProbleme->carte[i][j].motif);
        }
        fscanf(pCarte, "%c", &c);
    }
    pProbleme->carte[pProbleme->depart.x][pProbleme->depart.y].motif = 'D';
    pProbleme->carte[pProbleme->arrivee.x][pProbleme->arrivee.y].motif = 'A';
}


void afficheCarte(Probleme * pProbleme) {
    printf("Position de depart : \tD(%i;%i)\n",pProbleme->depart.x, pProbleme->depart.y);
    printf("Position d'arrivee : \tA(%i;%i)\n",pProbleme->arrivee.x, pProbleme->arrivee.y);
    printf("Nombre de lignes : \t%i \nNombre de colonnes : \t%i\n", pProbleme->nbLignes, pProbleme->nbColonnes);
    printf("\n");
    for (int i = 0; i < pProbleme->nbLignes; i++) {
        for (int j = 0; j < pProbleme->nbColonnes; j++) {
            printf("%c", pProbleme->carte[i][j].motif);
        }
        printf("\n");
    }
}


void libereCarte(Probleme * pProbleme) {
    for (int i = 0; i < pProbleme->nbLignes; i++) {
       free(pProbleme->carte[i]);
    }
    free(pProbleme->carte);
}
