#ifndef H_CARTE_INCLUDED
#define H_CARTE_INCLUDED

#include <stdbool.h>

typedef struct Case {
    unsigned int x, y;
    char motif;
} Case;


typedef struct Probleme {
    Case depart, arrivee;
    unsigned int nbLignes, nbColonnes;
    Case **carte;
} Probleme;


void lireCarte(FILE * pCarte, Probleme * pProbleme);
void afficheCarte(Probleme * pProbleme);
void libereCarte(Probleme * pProbleme);
void lireCase(FILE * pCarte, Case * pCase);

#endif // H_CARTE_INCLUDED
