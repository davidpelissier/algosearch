﻿#ifndef H_NOEUD_INCLUDED
#define H_NOEUD_INCLUDED


/**
 * \struct Noeud
 * \brief Objet `Noeud` générique.
 *
 * `Noeud` est un objet représentant une noeud. Ce dernier possède un contenu
 * générique permettant à l'utilisateur d'incorporer n'importe quel type de
 * données.
 * Chaque noeud contient également une référence vers son successeur, ce qui
 * permet son utilisation au sein de structures dynamiques telles que des listes
 * chaînées, des piles ou encore des files.
 *
 * \see h_file.h
 * \see h_liste.h
 */
typedef struct Noeud {
    void * contenu;             /*!< Contenu générique d'un noeud. */
    struct Noeud * pere;        /*!< pointeur vers le `Noeud` pere. */
    struct Noeud * successeur;  /*!< pointeur vers le `Noeud` successeur. */
} Noeud;

#endif // H_NOEUD_INCLUDED
