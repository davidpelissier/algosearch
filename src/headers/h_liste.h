﻿/**
 * \file h_liste.h
 * \brief Classe modélisant un objet de type `Liste` simplement chaînée
 *        dynamique générique.
 *
 * Classe permettant de modéliser, sous forme de structure, un objet de type
 * liste simplement chaînée.
 * La liste est composée de `noeuds` génériques et est dynamique. De ce fait, un
 * attribut `taille` permet de connaître, à tout instant, le nombre d'éléments
 * contenus dans la liste.
 *
 * \see h_noeud.h
 */

#ifndef H_LISTE_INCLUDED
#define H_LISTE_INCLUDED

#include <stdlib.h>
#include <stdbool.h>
#include "h_noeud.h"


/**
 * \struct Liste
 * \brief Objet `Liste` simplement chaînée dynamique générique.
 *
 * `Liste` est un objet représentant une liste simplement châinée. Cette dernière
 * est composée de `noeuds` génériques.
 * L'objet `Liste` à proprement parlé, contient uniquement la référence vers son
 * premier élément. Chaque noeuds possède une référence vers le noeud suivant,
 * de ce fait, la liste est dite simplement chaînée.
 *
 * \see h_noeud.h
 */
typedef struct Liste {
    Noeud * premierElem;    /*!< Premier élément de la liste. */
    int taille;             /*!< Taille de la liste. */
} Liste;


/**
 * \fn Liste creerListe(void)
 * \brief Crée et initialise un objet `Liste`.
 *
 * Crée et initialise un objet `Liste`.
 *
 * \return Retourne un objet `Liste` créé et initialisé.
 */
Liste creerListe(void);


/**
 * \fn void ajouterDebListe(Noeud * pElem, Liste * pListe)
 * \brief Ajoute un élément passé en paramètre en début d'un objet `Liste`.
 *
 * Ajoute un élément passé en paramètre en début d'un objet `Liste`.
 *
 * \param pElem Objet `Noeud` à ajouter dans la liste.
 * \param pListe Objet `Liste` dans lequel on désire insérer l'élément.
 */
void ajouterDebListe(Noeud * pElem, Liste * pListe);


/**
 * \fn void ajouterFinListe(Noeud * pElem, Liste * pListe)
 * \brief Ajoute un élément passé en paramètre en fin d'un objet `Liste`.
 *
 * Ajoute un élément passé en paramètre en fin d'un objet `Liste`.
 *
 * \param pElem Objet `Noeud` à ajouter dans la liste.
 * \param pListe Objet `Liste` dans lequel on désire insérer l'élément.
 */
void ajouterFinListe(Noeud * pElem, Liste * pListe);


/**
 * \fn Noeud* iElemListe(int pIndice, Liste * pListe)
 * \brief Recupère l'élément présent à l'indice `pIndice` passé en paramètre, au
 *        sein d'un objet `Liste`.
 *
 * Recupère l'élément présent à l'indice `pIndice` passé en paramètre, au
 * sein d'un objet `Liste`.
 *
 * \param pIndice Indice de l'élément de type `Noeud` à récupérer dans la liste.
 * \param pListe Objet `Liste` dans lequel on dérsire récupérer l'élément.
 */
Noeud* iElemListe(int pIndice, Liste * pListe);


/**
 * \fn bool estListeVide(Liste * pListe)
 * \brief Indique si un objet `Liste` est vide ou non.
 *
 * Indique si un objet `Liste` est vide ou non.
 *
 * \param pFile Objet `Liste` que l'on souhaite tester.
 * \return `true (1)` si la liste est vide, `false (0)` sinon.
 */
bool estListeVide(Liste * pListe);

bool estPresentListe(Noeud * pElem, Liste * pListe);

#endif // H_LISTE_INCLUDED
