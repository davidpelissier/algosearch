﻿/**
 * \file h_recherche.h
 * \brief Librairie d'algorithmes de recherche génériques.
 *
 * Librairie contenant un ensemble d'algorithmes permettant de résoudre des
 * problèmes de recherche génériques.
 *
 * \see h_noeud.h
 */

#ifndef H_RECHERCHE_INCLUDED
#define H_RECHERCHE_INCLUDED

#include <stdlib.h>
#include <stdbool.h>
#include "h_file.h"
#include "h_liste.h"
#include "h_noeud.h"


/**
 * \fn bool parcoursLargeur(Noeud * pDepart, Noeud * pSolution,
 *                          bool (*estSolution)(Noeud *, Noeud *),
 *                          void (*recupSucc)(Noeud *, File *))
 * \brief Parcours en largeur d'un problème.
 *
 * Parcourt, à la manière d'une recherche au sein d'un arbre, un problème
 * contenu dans un objet `File`.
 * Cette file contiendra des `Noeuds` représentant, par exemple, un état d'un
 * graphe ou encore un noeud d'un arbre.
 * Cette algorithme parcourt le problème en largeur et détermine uniquement si
 * ce dernier possède une solution ou non.
 *
 * \param pDepart Objet `Noeud` qui constitue la racine de notre arbre de
 *                parcours, autrement dit, le départ du problème à résoudre.
 * \param pSolution Objet `Noeud` qui constitue la solution du problème à
 *                  résoudre.
 * \param estSolution Pointeur sur fonction qui permet de déterminer si le noeud
 *                    est solution du problème.
 * \param recupSucc Pointeur sur fonction qui permet de récupérer les
 *                  successeurs d'un noeud.
 * \return `true (1)` si le problème possède une solution, `false (0)` sinon.
 */
bool solutionLargeur(Noeud * pDepart, Noeud * pSolution,
                     void * pProbleme,
                     bool (*estSolution)(Noeud *, Noeud *),
                     void (*recupSucc)(Noeud *, File *, void * pProbleme));

Liste cheminLargeur(Noeud * pDepart, Noeud * pSolution,
                     void * pProbleme,
                     bool (*estSolution)(Noeud *, Noeud *),
                     void (*recupSucc)(Noeud *, File *, void * pProbleme));

#endif // H_RECHERCHE_INCLUDED
