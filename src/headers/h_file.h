﻿/**
 * \file h_file.h
 * \brief Classe modélisant un objet de type `File` dynamique générique.
 *
 * Classe permettant de modéliser, sous forme de structure, un objet de type
 * file FIFO (First In, First Out).
 * Cette dernière présentera donc une tête et une queue, représentant
 * respectivement la sortie et l'entrée de la file.
 * La file est composée de `noeuds` génériques et est dynamique. De ce fait, un
 * attribut `taille` permet de connaître, à tout instant, le nombre d'éléments
 * contenus dans la file.
 *
 * \see h_noeud.h
 */

#ifndef H_FILE_INCLUDED
#define H_FILE_INCLUDED

#include <stdlib.h>
#include <stdbool.h>
#include "h_noeud.h"


/**
 * \struct File
 * \brief Objet `File` dynamique générique.
 *
 * `File` est un objet représentant une file. Cette dernière est composée de
 * `noeuds` génériques. Elle est caractérisée par une `tete` et une `queue`
 * désignant, respectivement, les points de sortie et d'entrée des noeuds dans
 * la file.
 *
 * \see h_noeud.h
 */
typedef struct File {
    Noeud *tete;    /*!< Tête de la file. */
    Noeud *queue;   /*!< Queue de la file. */
    int taille;     /*!< Taille de la file. */
} File;


/**
 * \fn File creerFile(void)
 * \brief Crée et initialise un objet `File`.
 *
 * \return Retourne un objet `File` créé et initialisé.
 */
File creerFile(void);


/**
 * \fn void viderFile(File * pFile)
 * \brief Vide un objet `File`.
 *
 * \param pFile Objet `File` à vider.
 */
void viderFile(File * pFile);


/**
 * \fn void ajouterFile(Noeud * pElem, File * pFile)
 * \brief Ajoute un élément passé en paramètre en tête d'un objet `File`.
 *
 * \param pElem Objet `Noeud` à ajouter dans la file.
 * \param pFile Objet `File` dans lequel on désire insérer l'élément.
 */
void ajouterFile(Noeud * pElem, File * pFile);


/**
 * \fn bool retirerFile(Noeud * pElem, File * pFile)
 * \brief Retire l'élément présent en queue d'un objet `File`.
 *
 * \param pElem Objet `Noeud` retiré de la file.
 * \param pFile Objet `File` dans lequel on désire retirer l'élément.
 * \return `true (1)` si le retrait s'est déroulé correctement, `false (0)` sinon.
 */
Noeud * retirerFile(File * pFile);


/**
 * \fn bool estFileVide(File * pFile)
 * \brief Indique si un objet `File` est vide ou non.
 *
 * \param pFile Objet `File` que l'on souhaite tester.
 * \return `true (1)` si la file est vide, `false (0)` sinon.
 */
bool estFileVide(File * pFile);

#endif // H_FILE_INCLUDED
