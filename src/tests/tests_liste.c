#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "../headers/h_noeud.h"
#include "../headers/h_liste.h"

#define TAILLE 5

void testsListe(void) {
    // Cr�ation d'une liste pour les tests de liste
    Liste li = creerListe();

    // D�claration d'un tableau de valeurs � ajouter � la liste
    char valListe[TAILLE];

    // initialisation du tableau de valeurs
    for(int i = 0; i < TAILLE; i++) {
        valListe[i] = 65 + i;
    }

    // Allocation dynamique des noeuds
    Noeud * un      =(Noeud*) malloc( sizeof(Noeud));
    Noeud * deux    =(Noeud*) malloc( sizeof(Noeud));
    Noeud * trois   =(Noeud*) malloc( sizeof(Noeud));
    Noeud * quatre  =(Noeud*) malloc( sizeof(Noeud));
    Noeud * cinq    =(Noeud*) malloc( sizeof(Noeud));
    Noeud * aAfficher;
    un->contenu     = &(valListe[0]);
    deux->contenu   = &(valListe[1]);
    trois->contenu  = &(valListe[2]);
    quatre->contenu = &(valListe[3]);
    cinq->contenu   = &(valListe[4]);

    // Test de la fonction 'estListeVide()'
    printf("\n---- Test de la fonction estListeVide() ---- ");
    if(estListeVide(&li)) {
        printf("\nLa liste est vide.");
    } else {
        printf("\nLa liste n'est pas vide (taille : %i).", li.taille);
    }

    // Test de la fonction 'ajoutDebListe()'
    printf("\n\n---- Test de la fonction ajouterDebListe() ---- ");
    printf("\nAjout des noeuds A, B puis C en debut de liste");
    ajouterDebListe(un, &li);
    ajouterDebListe(deux, &li);
    ajouterDebListe(trois, &li);

    // Affichage du contenu de la liste
    printf("\n\nContenu de la liste : \n\t");
    aAfficher = li.premierElem;
    for(int i = 0; i < li.taille; i++) {
        printf(" %c  -> ", *((int*)aAfficher->contenu));
        aAfficher = aAfficher->successeur;
    }

    // Test de la fonction 'estPresentListe()'
    printf("\n\n---- Test de la fonction estPresentListe() ---- ");
    if(estPresentListe(un, &li)) {
        printf("\nA est present dans la liste.");
    } else {
        printf("\nA n'est pas present dans la liste.");
    }

    // Test de la fonction 'estListeVide()'
    printf("\n\n---- Test de la fonction estListeVide() ---- ");
    if(estListeVide(&li)) {
        printf("\nLa liste est vide.");
    } else {
        printf("\nLa liste n'est pas vide (taille : %i).", li.taille);
    }

    // Test de la fonction 'iElemListe()'
    printf("\n\n---- Test de la fonction iElemListe() ---- ");
    aAfficher = iElemListe(3, &li);
    printf("\nElement 3 :\t %c", *((int*)aAfficher->contenu));

    // Ajout des noeuds 4 et 5 � la liste
    printf("\n\n---- Test de la fonction ajouterFinListe() ---- ");
    printf("\nAjout des noeuds D puis E en fin de liste");
    ajouterFinListe(quatre, &li);
    ajouterFinListe(cinq, &li);

    // Affichage du contenu de la liste
    printf("\n\nContenu de la liste : \n\t");
    aAfficher = li.premierElem;
    for(int i = 0; i < li.taille; i++) {
        printf(" %c  -> ", *((int*)aAfficher->contenu));
        aAfficher = aAfficher->successeur;
    }
    printf("\n\n");

    free(aAfficher);
    aAfficher = NULL;
}
