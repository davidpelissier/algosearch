#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "../headers/h_noeud.h"
#include "../headers/h_file.h"

#define TAILLE 5

void testsFile(void) {
    // Cr�ation d'une file pour les tests de file
    File f = creerFile();

    // D�claration d'un tableau de valeurs � ajouter � la file
    char valListe[TAILLE];

    // initialisation du tableau de valeurs
    for(int i = 0; i < TAILLE; i++) {
        valListe[i] = 65 + i;
    }

    // Allocation dynamique des noeuds
    Noeud * un      =(Noeud*) malloc( sizeof(Noeud));
    Noeud * deux    =(Noeud*) malloc( sizeof(Noeud));
    Noeud * trois   =(Noeud*) malloc( sizeof(Noeud));
    Noeud * quatre  =(Noeud*) malloc( sizeof(Noeud));
    Noeud * cinq    =(Noeud*) malloc( sizeof(Noeud));
    Noeud * aAfficher;
    un->contenu     = &(valListe[0]);
    deux->contenu   = &(valListe[1]);
    trois->contenu  = &(valListe[2]);
    quatre->contenu = &(valListe[3]);
    cinq->contenu   = &(valListe[4]);

    // Test de la fonction 'estFileVide()'
    printf("\n---- Test de la fonction estFileVide() ---- ");
    if(estFileVide(&f)) {
        printf("\nLa File est vide.");
    } else {
        printf("\nLa File n'est pas vide (taille : %i).", f.taille);
    }

    // Test de la fonction 'ajouterFile()'
    printf("\n\n---- Test de la fonction ajouterFile() ---- ");
    printf("\nAjout des noeuds A, B puis C a la file");
    ajouterFile(un, &f);
    ajouterFile(deux, &f);
    ajouterFile(trois, &f);

    // Affichage du contenu de la file
    printf("\n\nContenu de la file : \n\t");
    aAfficher = f.tete;
    for(int i = 0; i < f.taille; i++) {
        printf(" %c  -> ", *((int*)aAfficher->contenu));
        aAfficher = aAfficher->successeur;
    }

    // Test de la fonction 'estFileVide()'
    printf("\n---- Test de la fonction estFileVide() ---- ");
    if(estFileVide(&f)) {
        printf("\nLa File est vide.");
    } else {
        printf("\nLa File n'est pas vide (taille : %i).", f.taille);
    }

    // Test de la fonction 'retirerFile()'
    printf("\n\n---- Test de la fonction retirerFile() ---- ");
    Noeud * retire = retirerFile(&f);
    if ( retire != NULL ) {
        printf("\nLe noeud %c a bien ete retire de la file.", *((int *)retire->contenu));
        printf("\nContenu de la file : \n\n\t");
        aAfficher = f.tete;
        for(int i = 0; i < f.taille; i++) {
            printf(" %c  -> ", *((int *)aAfficher->contenu));
            aAfficher = aAfficher->successeur;
        }
    } else {
        printf("\nLe noeud n'a pas pu etre retire de la file.");
    }

    retire = retirerFile(&f);
    if ( retire != NULL ) {
        printf("\nLe noeud %c a bien ete retire de la file.", *((int *)retire->contenu));
        printf("\nContenu de la file : \n\n\t");
        aAfficher = f.tete;
        for(int i = 0; i < f.taille; i++) {
            printf(" %c  -> ", *((int *)aAfficher->contenu));
            aAfficher = aAfficher->successeur;
        }
    } else {
        printf("\nLe noeud n'a pas pu etre retire de la file.");
    }

    printf("\nAjout des noeuds D puis E a la file");
    ajouterFile(quatre, &f);
    ajouterFile(cinq, &f);

    // Affichage du contenu de la file
    printf("\n\nContenu de la file : \n\t");
    aAfficher = f.tete;
    for(int i = 0; i < f.taille; i++) {
        printf(" %c  -> ", *((int*)aAfficher->contenu));
        aAfficher = aAfficher->successeur;
    }

    // Test de la fonction 'viderFile()'
    printf("\n\n---- Test de la fonction viderFile() ---- ");

    if(estFileVide(&f)) {
        printf("\nLa File est vide.");
    } else {
        printf("\nLa File n'est pas vide (taille : %i).", f.taille);
    }

    viderFile(&f);

    // Test de la fonction 'estFileVide()'
    printf("\nLancement de la fonction 'viderFile()' ");
    if(estFileVide(&f)) {
        printf("\nLa File est vide.");
    } else {
        printf("\nLa File n'est pas vide (taille : %i).", f.taille);
    }

    printf("\n\n");

    free(aAfficher);
    aAfficher = NULL;
}
