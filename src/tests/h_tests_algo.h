#ifndef H_TESTS_ALGO_INCLUDED
#define H_TESTS_ALGO_INCLUDED

void testsAlgo(void);
bool estSolution(Noeud *, Noeud *);
void recupSucc(Noeud *, File *);

#endif // H_TESTS_ALGO_INCLUDED
