#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>

#include "../headers/h_carte.h"
#include "../headers/h_noeud.h"
#include "../headers/h_file.h"
#include "../headers/h_liste.h"
#include "../headers/h_recherche.h"

bool estSolution(Noeud * pNoeud1, Noeud * pNoeud2);
void recupSucc(Noeud * pCourant, File * pFileSuccesseurs, void* pProbleme);

void testsAlgo(void) {
    FILE* carte;
    carte = fopen("carte.txt", "r" );

    if(carte == NULL) {
        printf("\n Erreur ouverture fichier\n");
    } else {
        clock_t debut, fin;
        debut = clock();
        Probleme probleme;
        lireCarte(carte, &probleme);
        afficheCarte(&probleme);
        Noeud * depart  = (Noeud *) malloc(sizeof(Noeud));
        Noeud * arrivee = (Noeud *) malloc(sizeof(Noeud));
        Noeud * aAfficher = (Noeud *) malloc(sizeof(Noeud));
        depart->contenu  = &probleme.depart;
        arrivee->contenu = &probleme.arrivee;
        //bool solution = solutionLargeur(depart, arrivee, &probleme, estSolution, recupSucc);
        Liste chemin  = cheminLargeur(depart, arrivee, &probleme, estSolution, recupSucc);
        fin = clock();

        if (chemin.taille > 0) {
            printf("\n\nUne solution au probleme a ete trouve (en %f secondes).\n", (double) (fin - debut)/CLOCKS_PER_SEC);
            printf("\n\t\tChargement de la solution ...");

            aAfficher = chemin.premierElem;
            while(aAfficher->pere != NULL) {
                Case * aModifier = (Case*) aAfficher->contenu;
                probleme.carte[aModifier->x][aModifier->y].motif = '*';
                aAfficher = aAfficher->pere;
            }
            probleme.arrivee.motif = 'A';
            _sleep(2*1000);
            system("cls");
            afficheCarte(&probleme);

        } else {
            printf("\n\nAucune solution trouvee (%f secondes).\n", (double) (fin - debut)/CLOCKS_PER_SEC);
        }
        libereCarte(&probleme);
    }
}


bool estSolution(Noeud * pNoeud1, Noeud * pNoeud2) {
    Case* pt_position1 = (Case*) pNoeud1->contenu;
    Case* pt_position2 = (Case*) pNoeud2->contenu;

    if (pt_position1->x == pt_position2->x
     && pt_position1->y == pt_position2->y) {
        return true;
    } else {
        return false;
    }
}


void recupSucc(Noeud * pCourant, File * pFileSuccesseurs, void* pProbleme) {
    Probleme* pt_probleme = (Probleme *) pProbleme;
    Noeud * nouvelElem    = (Noeud *) malloc(sizeof(Noeud));
    // R�cup�ration des coordonn�es du noeud courant
    Case* caseCourante    = (Case*) pCourant->contenu;
    Case* nouvelleCase;

    // R�cup�ration des d�placements possibles � partir du noeud courant
    // en x, � gauche :
    if (   caseCourante->x-1 >= 0
        && caseCourante->x-1 < pt_probleme->nbLignes) {
        Case* nouvelleCase = &pt_probleme->carte[caseCourante->x-1][caseCourante->y];
        if (nouvelleCase->motif == ' ' || nouvelleCase->motif == 'A') {
            nouvelElem->contenu = (Case*) nouvelleCase;
            nouvelElem->pere    = pCourant;
            ajouterFile(nouvelElem, pFileSuccesseurs);
        }
    }
    // en x, � droite :
    if (   caseCourante->x+1 >= 0
        && caseCourante->x+1 < pt_probleme->nbLignes) {
        nouvelleCase = &pt_probleme->carte[caseCourante->x+1][caseCourante->y];
        if (nouvelleCase->motif == ' ' || nouvelleCase->motif == 'A') {
            nouvelElem->contenu = (Case*) nouvelleCase;
            nouvelElem->pere    = pCourant;
            ajouterFile(nouvelElem, pFileSuccesseurs);
        }
    }
    // en y, en haut :
    if (   caseCourante->y-1 >= 0
        && caseCourante->y-1 < pt_probleme->nbColonnes) {
        nouvelleCase = &pt_probleme->carte[caseCourante->x][caseCourante->y-1];
        if (nouvelleCase->motif == ' ' || nouvelleCase->motif == 'A') {
            nouvelElem->contenu = (Case*) nouvelleCase;
            nouvelElem->pere    = pCourant;
            ajouterFile(nouvelElem, pFileSuccesseurs);
        }
    }
    // en y, en bas :
    if (   caseCourante->y+1 >= 0
        && caseCourante->y+1 < pt_probleme->nbColonnes) {
        nouvelleCase = &pt_probleme->carte[caseCourante->x][caseCourante->y+1];
        if (nouvelleCase->motif == ' ' || nouvelleCase->motif == 'A') {
            nouvelElem->contenu = (Case*) nouvelleCase;
            nouvelElem->pere    = pCourant;
            ajouterFile(nouvelElem, pFileSuccesseurs);
        }
    }
}
